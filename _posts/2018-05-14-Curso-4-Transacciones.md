---
layout: presentacion
title: Curso 4 - Transacciones
description: Transacciones en bases de datos
theme: white
transition: slide
date: 2018-05-14 9:00:00 -0400
---

<section data-markdown>
## Seguridad en bases de datos relacionales

EMI - 14/05/2018

### Curso 4 - Transacciones en bases de datos
</section>

<section data-markdown>
## Charla del día

# ¿🐙?
</section>

<section data-markdown>
## Examen parcial nº1

</section>

<section data-markdown>
## Transacciones

- Una transacción hacer pasar una base de datos de un estado A a un estado B.
- Puede corresponder a varias operaciones sucesivas.
- Todas las operaciones tienen que realizarse para pasar al estado B, sino se cancela la transacción y la base de datos se queda en el estado A.
</section>

<section data-markdown>
## Ejemplo de transacción

- Las dos siguientes operaciones son una sola transacción

```sql
UPDATE accounts SET balance = balance - 100.00
    WHERE name = 'Alice';
UPDATE accounts SET balance = balance + 100.00
    WHERE name = 'Bob';
```

- En caso de interrumpirse luego de la primera operación (por alguna falla del sistema), se "cancela" la primera operación.
</section>

<section data-markdown>
## Ejemplo de transacción

- Una simple operación también es una transacción, por ejemplo

```sql
DELETE FROM accounts
    WHERE name = 'Alice';
```
</section>

<section data-markdown>
## ACID

- Las cuatro propiedades "ACID" garantizan que una transacción informática se ejecuta de manera confiable:

 - atomicidad (*Atomicity*)
 - coherencia (*Coherence*)
 - aislamiento (*Isolation*)
 - durabilidad (*Durability*)

</section>

<section data-markdown>
## Atomicidad ⚛

- 📜 Viene de *ατομος* (atomos) que significa: « que no se puede dividir ».
- Una o varias operaciones forman una transacción **atómica** si **aparecen como una sola** al resto del sistema.
- "todo o nada": se ejecuta completamente, o se vuelve al estado inicial.

</section>

<section data-markdown>
## Atomicidad ⚛

```sql
UPDATE accounts SET balance = balance - 100.00
    WHERE name = 'Alice';

🔥 ERROR 🔥 ERROR 🔥

UPDATE accounts SET balance = balance + 100.00
    WHERE name = 'Bob';
```

- No se respeta la atomicidad porque la segunda operación no llega a realizarse, y debería cancelarse la primera operación.

</section>


<section data-markdown>
## Coherencia ✅

- Una transacción es **coherente** si lleva la base de datos de un estado válido a otro estado válido.
- Tiene que verificar todas las reglas definidas en la base de datos, como las restricciones por ejemplo: claves foráneas, unicidad de clave primaria, campo "NOT NULL".
- Significa también que una transacción realizada en el futuro ve los efectos de una transacción realizada en el pasado (difícil de garantizar en una base de datos distribuida).

</section>

<section data-markdown>
## Coherencia ✅

```sql
UPDATE accounts SET balance = balance - 100.00
    WHERE name = 'Alice';

🔥 ERROR 🔥 ERROR 🔥

UPDATE accounts SET balance = balance + 100.00
    WHERE name = 'Bob';
```

- Suponiendo que la suma de todas las cuentas tiene que ser igual a cero, no se respeta la coherencia porque al interrumpirse luego de la primera operación, la suma de las cuentas ya no iguala cero. Debería cancelarse la primera operación.

</section>


<section data-markdown>
## Aislamiento ⚡🧱⚡

- Dos transacciones están **aisladas** si su ejecución concurrente (o paralela) lleva al mismo resultado que su ejecución secuencial.
- Significa que dos transacciones no puede intercambiar datos, tener influencia mutua, ni ver sus estados internos.
- Existen cuatro niveles de aislamiento, del más estricto al más laxo (ver curso sobre "concurrencia").

</section>

<section data-markdown>
## Aislamiento ⚡🧱⚡

```sql
UPDATE accounts SET balance = balance - 100.00
    WHERE name = 'Alice';
UPDATE accounts SET balance = balance - 30.00
    WHERE name = 'Bob';
UPDATE accounts SET balance = balance + 30.00
    WHERE name = 'Alice';
(operación 2 validada)

🔥 ERROR 🔥 ERROR 🔥

UPDATE accounts SET balance = balance + 100.00
    WHERE name = 'Bob';
(operación 1 validada)
```

- Dos transacciones se mezclan, y al interrumpirse antes de la segunda operación de la transacción de 100.00 Bs, no es posible revertir las tres últimas operaciones porque ya se ha validado la transacción de 30.00 Bs.

</section>


<section data-markdown>
## Durabilidad 🖴

- Una transacción es **duradera** (o permanente) si se asegura que, una vez terminada la transacción, su resultado está guardado de manera permanente.
- Significa que si el sistema sufre fallas, las transacciones pasadas seguirán siendo accesibles y guardadas.
- Implica generalmente guardar en un medio no-volatil.

</section>

<section data-markdown>
## Durabilidad 🖴

```sql
UPDATE accounts SET balance = balance - 100.00
    WHERE name = 'Alice';
UPDATE accounts SET balance = balance + 100.00
    WHERE name = 'Bob';

🔥 ERROR 🔥 ERROR 🔥

Escribir de la memoria RAM al disco duro
```

- Aunque la transacción terminó, el sistema falla antes de haber escrito el nuevo estado de manera permanente. Al reiniciar, no hay huella de esta transacción.

</section>

<section data-markdown>
## Transacciones en SQL

- Las transacciones SQL utilizan varios comandos:

  - `START TRANSACTION` (PostGreSQL acepta también el alias `BEGIN`): iniciar una transacción
  - `COMMIT`: guardar el resultado de la transacción de manera permanente (utilizado en caso de éxito de la transacción)
  - `ROLLBACK`: parar la transacción y revertir todas las operaciones anteriores de la transacción (utilizado en caso de error en la transacción)

</section>

<section data-markdown>
## Ejemplo de transacción

```sql
START TRANSACTION;
UPDATE accounts SET balance = balance + 100.00
  WHERE name = 'Bob';
UPDATE accounts SET balance = balance - 100.00
  WHERE name = 'Alice';
COMMIT;
```

</section>

<section data-markdown>
## 🐘 Crear una tabla

- Convención: en lo que sigue, si una línea inicia por `$`, es para teclear en la línea de comando, sino es dentro de psql
- Crear la base de datos, e ingresar a la base

```bash
$ createdb -U slesage -W transacciones
$ psql -U slesage -W transacciones
```

- Crear la tabla `accounts` y llenarla con dos registros

```sql
CREATE TABLE accounts (name varchar, balance int);
INSERT INTO accounts (name, balance) VALUES ('Alice', 200), ('Bob', 300);
```

- Ver el estado de la tabla

```sql
SELECT * FROM accounts;
```
</section>

<section data-markdown>
## 🐘 Probar una transacción simple

- Transacción SQL para el intercambio de 100.00 de Alice hacia Bob

```sql
START TRANSACTION;
UPDATE accounts SET balance = balance + 100.00 WHERE name = 'Bob';
UPDATE accounts SET balance = balance - 100.00 WHERE name = 'Alice';
COMMIT;
```

- Ver el estado de la tabla

```sql
SELECT * FROM accounts;
```
</section>

<section data-markdown>
## 🐘 Falla de una transacción

- Agregamos una restricción que prohíbe los saldos negativos

```sql
ALTER TABLE accounts ADD CONSTRAINT positivo CHECK (balance >= 0);
```

- Probamos de nuevo, de manera reiterada

```sql
START TRANSACTION;
UPDATE accounts SET balance = balance + 100.00 WHERE name = 'Bob';
UPDATE accounts SET balance = balance - 100.00 WHERE name = 'Alice';
COMMIT;
```
</section>

<section data-markdown>
## 🐘 Usar transacción para prueba

- Se puede usar `ROLLBACK` para realizar una prueba, pero sin efecto real

```sql
START TRANSACTION;
ALTER TABLE accounts DROP CONSTRAINT positivo;
UPDATE accounts SET balance = balance - 1000000.00 WHERE name = 'Bob';
UPDATE accounts SET balance = balance + 1000000.00 WHERE name = 'Alice';
SELECT * FROM accounts;
ROLLBACK;
```
</section>

<section data-markdown>
## Puntos intermedios

- Las transacciones SQL también permiten usar etapas intermedias, o puntos intermedios, llamadas `SAVEPOINT`:

  - `SAVEPOINT`: crea un punto intermedio
  - `ROLLBACK TO SAVEPOINT`: parar la transacción y revertir todas las operaciones posteriores al punto intermedio
  - `RELEASE SAVEPOINT`: borrar el punto intermedio

- Su objetivo es dar seguridad al momento de "explorar" los datos.
</section>

<section data-markdown>
## 🐘 Usar los puntos intermedios

- Un ejemplo de punto intermedio, que permite revertir solo una parte de la transacción

```sql
START TRANSACTION;
SAVEPOINT sin_restriccion;
ALTER TABLE accounts DROP CONSTRAINT positivo;
UPDATE accounts SET balance = balance + 100.00 WHERE name = 'Bob';
UPDATE accounts SET balance = balance - 100.00 WHERE name = 'Alice';
SELECT * FROM accounts;
ROLLBACK TO SAVEPOINT sin_restriccion;
UPDATE accounts SET balance = balance - 100.00 WHERE name = 'Bob';
UPDATE accounts SET balance = balance + 100.00 WHERE name = 'Alice';
SELECT * FROM accounts;
COMMIT;
```
</section>
