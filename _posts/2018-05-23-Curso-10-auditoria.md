---
layout: presentacion
title: Curso 10 - Auditoría
description: auditoría
theme: white
transition: slide
date: 2018-05-22 9:00:00 -0400
---

<section data-markdown>
## Seguridad en bases de datos relacionales

EMI - 23/05/2018

### Curso 10 - Auditoría
</section>

<section data-markdown>
## 🏠 Diarios en CSV

- guardar el diario en formato CSV
- crear una tabla en la misma base de datos para cargar este diario (ver [Using CSV-Format Log Output](https://www.postgresql.org/docs/10/static/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-CSVLOG))
- cargarlo
- realizar búsquedas básicas
  - ¿cuántos eventos guardados?
  - ¿cuántos eventos por minuto, y por nivel de gravedad?
</section>

<section data-markdown>
## 🏠 Auditoría sobre conexiones

- encontrar la forma de configurar los logs de PostgreSQL para que quede registrado quién se conectó en el caso siguiente:

```bash
$ psql -U alice -W transacciones
```

```SQL
SET ROLE adminbases;
DROP TABLE prueba;
```
</section>

<section data-markdown>
## 💖 Retroalimentación sobre el curso 💖
</section>

<section data-markdown>
## 📝 Examen final
</section>
