---
layout: presentacion
title: Curso 2 - Datos
description: Curso sobre los riesgos a tomar en cuenta al administrar datos. K-anonimity.
theme: white
transition: slide
date: 2018-05-09 8:00:00 -0400
---

<section data-markdown>
## Seguridad en bases de datos relacionales

EMI - 09/05/2018

### Curso 2 - Datos
</section>

<section data-markdown>
## Charla del día

# ¿🐇?
</section>

<section data-markdown>
## Estado y datos

El Estado se moderniza
  - interoperabilidad
  - digitalización
  - estándares
  - estadísticas e indicadores
  - datos abiertos
</section>

<section data-markdown>
## Retos

- seguridad de la información
- protección de datos (consentimiento, propiedad, finalidad de la colecta)
- evitar filtración, venta, abuso
- al contrario: evitar trabas a la modernización
- toma de decisiones en base a datos
</section>

<section data-markdown>
## Big data

Las 3 V:

- v...
- v...
- v...

</section>

<section data-markdown>
## Retos del aprendizaje automático

- calidad de los datos y de la colecta, sesgos, "binaridad" / sobre-aprendizaje
- seguridad (coches)
- transparencia de los algoritmos
- usos y abusos
</section>

<section data-markdown>
## Preconizaciones

Datos personales:

- minimización, finalidad
- privacy by design - cifrado, hash
- logs
- fragmentación
- tiempo de conservación
- limitación en el procesamiento
- control de acceso
- anonimización, agregación, adición de "ruido"
</section>

<section data-markdown>
## Técnicas de anonimización

Aplicar hashes a los datos personales (*PII*): https://beta.observablehq.com/@severo/owasp-2018-1-4-web-crypto-api

↬ Demostración [👩‍🔬](https://beta.observablehq.com/@severo/owasp-2018-1-4-web-crypto-api)

</section>
