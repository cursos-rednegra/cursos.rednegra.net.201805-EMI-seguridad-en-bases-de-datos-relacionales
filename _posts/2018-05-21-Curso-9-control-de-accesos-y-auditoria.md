---
layout: presentacion
title: Curso 9 - Control de accesos, auditoría
description: Control de accesos, auditoría
theme: white
transition: slide
date: 2018-05-21 9:00:00 -0400
---

<section data-markdown>
## Seguridad en bases de datos relacionales

EMI - 21/05/2018

### Curso 9 - Control de accesos, auditoría
</section>

<section data-markdown>
## Control de accesos - conexiones
</section>

<section data-markdown>
## Conexiones - autenticación

- Los conceptos de roles, privilegios, esquemas, son comunes a los SGBD SQL, y corresponden a la **autorización**.
- Pero se requiere también una capa de **autenticación**:
  - no es estándar, no está definido por SQL
  - cada SGBD implementa sus propias funcionalidades de autenticación

</section>

<section data-markdown>
## Conexiones en PostgreSQL

- un servidor PostgreSQL puede manejar dos tipos de conexiones
  - escuchando sobre una interfaz de red y un puerto TCP/IP, por defecto: localhost:5432 para la interfaz interna sobre el puerto 5432
    - en claro
    - cifrado, [con SSL/TLS](https://www.postgresql.org/docs/10/static/ssl-tcp.html), sin autenticación del cliente
    - o cifrado, con SSL/TLS, con autenticación del cliente mediante [certificado](https://www.postgresql.org/docs/10/static/ssl-tcp.html#SSL-CLIENT-CERTIFICATES)
  - escuchando localmente con tomas Unix (*socket*), por ejemplo /tmp/systemd-private-7bb52cdc5a3344b58415140224afe1ee-postgresql.service-Xy0D0X/tmp

</section>

<section data-markdown>
## Métodos de autenticación

- PostgreSQL propone varios [mecanismos de autenticación](https://www.postgresql.org/docs/10/static/auth-methods.html), entre ellos:
  - autenticación por contraseña (se recomienda nivel "scram-sha-256", y uso de SSL/TLS): usuario y contraseña
  - autenticación de pares (*peer*): solo localmente, prueba de conectarse con el usuario "Unix" corriente, que tiene que existir en la base de datos con el mismo nombre
  - autenticación por LDAP: como autenticación por contraseña, pero delegada a un servidor LDAP
  - autenticación por certificado SSL/TLS: el usuario no envía contraseña, solamente tiene que demostrar la posesión del certificado SSL registrado
</section>


<section data-markdown>
## 📝 Métodos de autenticación

- configurar el archivo [pg_hba.conf](https://www.postgresql.org/docs/10/static/auth-pg-hba-conf.html) (y reiniciar el servidor) para:
  - realizar una conexión por autenticación de pares (usuario "alice")
  - realizar una conexión por contraseña (usuario "alice")
  - probar de impedir a "bob" de conectarse, desde este archivo de configuración, verificar

En caso de problema de conexión, ver [la documentación](https://www.postgresql.org/docs/10/static/client-authentication-problems.html)
</section>

<section data-markdown>
## Temas de seguridad

- Evaluar el parámetro "password_encryption" en postgresql.conf (tipo de almacenamiento de las contraseñas)
  - Por defecto usa el valor "md5" (hash MD5)
  - Se recomienda cambiar a "scram-sha-256", pero hay que asegurarse que los clientes lo soporten
    - si se cambia, también modificar el mecanismo de autenticación a scram-sha-256 en pg_hba.conf
- Leer las advertencias sobre la [suplantación](https://www.postgresql.org/docs/10/static/preventing-server-spoofing.html)

</section>

<section data-markdown>
## Corrección examen parcial 2

</section>

<section data-markdown>
## Auditoría

</section>

<section data-markdown>
## Auditoría - diarios

- Las auditorías son trabajos de inspección *a posteriori*, por parte de una entidad autónoma, sobre un proceso operativo.
- Se realiza un análisis sobre documentos.
- En el caso de los sistemas y de las bases de datos, uno de los tipos de "documentos" utilizados para fines de auditorías son los diarios (*logs*).

```bash
$ sudo journalctl -f -u postgresql.service
```

```
may 20 16:56:45 manjaro-n56vz postgres[4257]: 2018-05-20 16:56:45.095 -04 [4271] LOG:  sentencia: CREATE TABLE importantisima_tabla (id INT);
may 20 16:57:04 manjaro-n56vz postgres[4257]: 2018-05-20 16:57:04.495 -04 [4271] LOG:  sentencia: INSERT INTO importantisima_tabla VALUES (1);
may 20 16:57:07 manjaro-n56vz postgres[4257]: 2018-05-20 16:57:07.111 -04 [4271] LOG:  sentencia: SELECT * FROM importantisima_tabla ;
may 20 16:57:09 manjaro-n56vz postgres[4257]: 2018-05-20 16:57:09.351 -04 [4271] LOG:  sentencia: DROP TABLE importantisima_tabla ;
```
</section>

<section data-markdown>
## (¿dónde están los logs?)

- según el sistema operativo, y la configuración de postgresql, los logs pueden encontrarse en lugares diferentes.
- en caso de problema, ver:
  - [Dear PostgreSQL: Where are my logs?](https://www.endpoint.com/blog/2014/11/12/dear-postgresql-where-are-my-logs)
  - [Documentación de PostgreSQL](https://www.postgresql.org/docs/10/static/runtime-config-logging.html)
</section>

<section data-markdown>
## ¿Qué colocar en los diarios?

- en los diarios se pueden colocar las huellas de varias acciones:
  - errores, advertencias y notificaciones
  - actividad del servidor (encender o apagar el servidor, tipos de conexiones abiertas, operaciones de mantenimiento)
  - comandos de "definición de datos" (DDL), como CREATE, ALTER, DROP
  - comandos de "modificación de datos" (DML), como INSERT, UPDATE, DELETE, TRUNCATE, COPY
  - comandos de "control de datos" (DCL), como GRANT, REVOKE

- en PostgreSQL, se define a través del parámetro [log_statement](https://www.postgresql.org/docs/10/static/runtime-config-logging.html#GUC-LOG-STATEMENT) de postgresql.conf.
</section>

<section data-markdown>
## ¿Qué colocar en los diarios?

- en los diarios, adicionalmente, se tiene que definir qué metadatos incluir en el prefijo de cada línea
  - conexión: usuario, aplicación, base de datos, IP
  - fecha y hora
  - número de transacción

- en PostgreSQL, se define a través del parámetro [log_line_prefix](https://www.postgresql.org/docs/10/static/runtime-config-logging.html#GUC-LOG-LINE-PREFIX) de postgresql.conf.
</section>

<section data-markdown>
## ¿Qué colocar en los diarios?

- en los diarios, finalmente, se tiene que definir el nivel de importancia de los eventos a registrar
  - DEBUG5, ..., DEBUG1: información detallada
  - INFO, NOTICE: información básica
  - WARNING, ERROR: problemas y errores
  - LOG: información de interés para administradores
  - FATAL, PANIC: errores que hacen caer la sesión o el servidor

- en PostgreSQL, se define a través de los parámetros de postgresql.conf:
  - [log_min_message](https://www.postgresql.org/docs/10/static/runtime-config-logging.html#GUC-LOG-MIN-MESSAGES): nivel mínimo para registrar el evento
  - [log_min_error_statement](https://www.postgresql.org/docs/10/static/runtime-config-logging.html#GUC-LOG-MIN-ERROR-STATEMENT): nivel mínimo para registrar el comando que originó el error
</section>

<section data-markdown>
## 📝 Configuración de logs

- configurar el servidor PostgreSQL para que guarde:
 - todos los errores, con el comando original,
 - todos los comandos de modificación de tablas,
 - en cada fila: el usuario conectado, y la hora

- verificar el funcionamiento correcto

</section>

<section data-markdown>
## Información adicional

- Es posible agregar información adicional, o a medida:
  - usando extensiones como [PGAudit](https://www.pgaudit.org/)
  - [creando funciones](https://wiki.postgresql.org/wiki/Audit_trigger) llamadas automáticamente para las acciones de interés, con [CREATE TRIGGER](https://www.postgresql.org/docs/10/static/sql-createtrigger.html), [CREATE FUNCTION](https://www.postgresql.org/docs/10/static/sql-createfunction.html), [RAISE](https://www.postgresql.org/docs/10/static/plpgsql-errors-and-messages.html)
  - añadiendo controles de integridad con [restricciones, claves primarias, foráneas](https://www.postgresql.org/docs/10/static/ddl-constraints.html), que generarán errores en caso de violación
- Puede ser útil, o hasta necesario, para casos complejos como

```SQL
  CREATE FUNCTION foo()
   RETURNS void LANGUAGE plpgsql AS
  $func$
  BEGIN
      EXECUTE 'CREATE TABLE import' || 'ant_table (id INT)';
  END $$;
```
</section>

<section data-markdown>
## ¿Qué información guardar?

- Generalmente no es posible guardar toda la actividad de un servidor de base de datos, porque la cantidad de mensajes producidos generaría problemas en
  - el costo del almacenamiento requerido
  - los recursos adicionales para procesarlos (sin afectar las operaciones normales)
  - la dificultad de separar la información útil de la "basura"

</section>

<section data-markdown>
## ¿Qué información guardar?

- se recomienda definir, a través de la política de seguridad de la información (metodología ISO 27000) qué son los riesgos, y usar los diarios como controles para mitigarlos
- en el concepto de mejora continua, es recomendable iniciar con un nivel de diario general, e ir afinándolo a medida que pase el tiempo, o cuándo surja una sospecha específica
- para la cantidad de datos
  - se debe recurrir a una rotación de los diarios, especificando un tiempo máximo de conservación ([log_truncate_on_rotation](https://www.postgresql.org/docs/10/static/runtime-config-logging.html#GUC-LOG-TRUNCATE-ON-ROTATION))
  - se aconseja comprimir los archivos de las diarios
  - se puede almacenar los diarios en un sistema externo, independiente, en una base de datos, y con capacidad de análisis (ver [Elastic Stack](https://www.elastic.co/elk-stack) por ejemplo)

</section>

<section data-markdown>
## Datos personales

- los diarios pueden contener información del mismo nivel de sensibilidad que los datos de las bases
- además, para fines de auditoría, los diarios deben ser protegidos contra las modificaciones por los administradores de las bases de datos
- se recomienda aplicar:
  - métodos de anonimización sobre los diarios
  - protección de los diarios (derechos de acceso, en el sistema operativo, o en el sistema que agrupa los diarios)
  - delegación de los diarios a un sistema independiente (Elastic Stack, logstash)
</section>

<section data-markdown>
## Formato de los diarios

- los diarios pueden estar
  - guardados en archivos de texto, con una línea por evento
  - guardados en archivos CSV, con una línea por evento, y una columna por tipo de información
  - delegados a otro sistema (syslog en GNU/Linux, eventlog en Windows), que puede guardar en sus propios archivos, o realizar otra delegación (por ejemplo a [logstash](https://www.elastic.co/guide/en/logstash/current/plugins-inputs-syslog.html))
- ver [log_destination](https://www.postgresql.org/docs/10/static/runtime-config-logging.html#GUC-LOG-DESTINATION) y toda la sección ["Where to log"](https://www.postgresql.org/docs/10/static/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-WHERE)
</section>

<section data-markdown>
## 📝 Diarios en CSV

- guardar el diario en formato CSV
- crear una tabla en la misma base de datos para cargar este diario (ver [Using CSV-Format Log Output](https://www.postgresql.org/docs/10/static/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-CSVLOG))
- cargarlo
- realizar búsquedas básicas
  - ¿cuántos eventos guardados?
  - ¿cuántos eventos por minuto, y por nivel de gravedad?
</section>

<section data-markdown>
## 👓 Experiencia de los/las maestrantes sobre auditoría de base de datos

</section>

<section data-markdown>
## 🏠 Ejercicio para el día miércoles

- encontrar la forma de configurar los logs de PostgreSQL para que quede registrado quién se conectó en el caso siguiente:

```bash
$ psql -U alice -W transacciones
```

```SQL
SET ROLE adminbases;
DROP TABLE prueba;
```

</section>


<section data-markdown>
## El día miércoles

- Corregiremos ejercicios
- Fin de auditoría
- Examen final
</section>
